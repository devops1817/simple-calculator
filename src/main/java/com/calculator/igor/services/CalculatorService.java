package com.calculator.igor.services;

import com.calculator.igor.utils.Utils;

public class CalculatorService {
	
	
	public static String execEquals(String text) throws Exception {
		
	
		//remove Spaces and replace ops
		String formatedText = Utils.removeSpacesAndReplaceOps(text);
		
		//String size
		int formatedTextLenght;
		
		//storage the actually digit
		char actuallyDigit;
		
		//Digits to be searched
		char firstDigit = '*';
		char secondDigit = '/';
		
		//booleans to confirm things
		boolean haveMultOrDiv;
		boolean haveSumOrSub;
		
		//result of operation
		String result;
		
		int cont  = 0; 
		
		try{
			do {
				
				formatedTextLenght = formatedText.length();
				haveMultOrDiv =  false;
				haveSumOrSub = false;
				result = "";
				
				int beginIndex = -1;
				int endIndex = -1;
				
				if(cont == 0) {
					haveSumOrSub = true;
				}
				
				//Search and exec mult and divs
				for(int i = 0; i < formatedTextLenght; i++) {
					actuallyDigit = formatedText.charAt(i);
					if((actuallyDigit == firstDigit || actuallyDigit == secondDigit) && i != 0) {
						
						if(firstDigit =='*') {
							haveMultOrDiv = true;
						}else {
							haveSumOrSub = true;
						}
					
						beginIndex = CalculatorService.searchBegin(i, formatedText);
						endIndex = CalculatorService.searchEnd(i, formatedText);
						result  = CalculatorService.whichOp(Double.parseDouble(formatedText.substring(beginIndex,i)),Double.parseDouble(formatedText.substring(i + 1,endIndex))
								,actuallyDigit);

						break;
						
					}
				}
				
				if(beginIndex != - 1) {
				
					if(beginIndex == 0) {
						formatedText = result + formatedText.substring(endIndex, formatedTextLenght);
					}else if(endIndex == formatedTextLenght) {
						formatedText = formatedText.substring(0, beginIndex) + result;
					}else {
						formatedText = formatedText.substring(0, beginIndex) + result + formatedText.substring(endIndex, formatedTextLenght);		
					}
				}
				
				if(!haveMultOrDiv) {
					firstDigit = '+';
					secondDigit = '-';
					cont ++;
				}
			
		}while(haveSumOrSub || haveMultOrDiv);
	}catch(Exception e){
		throw new Exception("ERROR");
	}


		
		return formatedText;
	}
	
	public static int searchBegin(int opIndex, String formatedText) {
		char actuallyDigit;
		for(int i = opIndex - 1; i > 0; i--) {
			actuallyDigit = formatedText.charAt(i);
			
			if(!Utils.isNumeric(actuallyDigit)) {
				return i + 1;
			}
		}
		return 0;
	}
	
	public static int searchEnd(int opIndex, String formatedText) throws Exception{
		char actuallyDigit;
		int cont = 0;
		
		for(int i = opIndex + 1; i < formatedText.length(); i++) {
			actuallyDigit = formatedText.charAt(i);
			if(!Utils.isNumeric(actuallyDigit)) {
				if(actuallyDigit != '-' || cont != 0) {
					return i;
				}
			}
			cont ++;
		}
		return formatedText.length();
	}
	
	public static String whichOp(double numOne, double numTwo, char op) throws Exception {
		
		if(op == '*') {
			return Utils.formatNumbers(CalculatorService.multiplication(numOne, numTwo));
		}
		
		if(op == '+') {
			return Utils.formatNumbers(CalculatorService.sum(numOne, numTwo));
		}
		
		if(op == '-') {
			
			return Utils.formatNumbers(CalculatorService.subtraction(numOne, numTwo));
			
		}else {
			return Utils.formatNumbers(CalculatorService.division(numOne, numTwo));
		}
		
		
		
	}
	
	public static Double sum(double numberOne, double numberTwo) {
		
		return numberOne + numberTwo;
	}
	public static Double subtraction(double numberOne, double numberTwo) {
		
		return numberOne - numberTwo;
		
	}
	public static Double multiplication(double numberOne, double numberTwo) {
		
		return numberOne * numberTwo;
		
	}
	public static Double division(double numberOne, double numberTwo) throws Exception {
		if(numberTwo == 0){
			throw new Exception("ERROR");
		}else{
			return numberOne / numberTwo;
	}
		
	}

}
