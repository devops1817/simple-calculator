package com.calculator.igor.utils;

import javafx.scene.control.TextField;

public class Utils {
	
	
	public static String formatNumbers(Double number) {
		
		if(number == number.intValue()) {
			return Integer.toString(number.intValue());
		}
		
		return Double.toString(number);
	}

	public static char findValueOfActionSource(String text) {
		return text.charAt(text.length() - 2);
	}

	public static boolean isNumeric(char digit) {
		if(digit == ',' || digit == '.') {
			return true;
		}
		try {

			Integer.parseInt(Character.toString(digit));

		} catch (NumberFormatException e) {
			return false;
		}

		return true;

	}
	
	public static String removeSpacesAndReplaceOps(String text) {
		text = text.replaceAll(" ","");
		text = text.replaceAll("x", "*");
		return text.replaceAll(",",".");
	}

	public static String returnFormatedString(String operator, String text) {
		String formatedText = "";
		
		if(!Utils.isNumeric(text.charAt(text.length() - 4))){
			return text;
		}
		
		if(operator.equals("-")) {
			return Utils.replaceOperatorSub(text);
		}
		
			
		for(int i = 0; i < text.length() -1; i++) {
			if(i == text.length() - 2) {
				formatedText += operator + " ";
			}else {
				formatedText += text.charAt(i);
				}
			}
		
		return formatedText;
	}


	private static String replaceOperatorSub(String text) {
		String newText = "";
		String aux = Character.toString(text.charAt(text.length() - 2));
		
		
		for(int i = 0; i < text.length() -1; i++) {
			if(i == text.length() - 2) {
				if(aux.equals("/") == true || aux.equals("x") == true) {
					newText += aux + " ";
					newText += " - ";
					break;
				}else {
					newText += "- ";
				}
			}else {
				newText += text.charAt(i);
			}
		}
		return newText;
	}

	
	public static boolean textFieldIsZero(String text) {
		if (text.equals("0")) {
			return true;
		}
		return false;
	}
	

}
