package com.calculator.igor.calculadora_simples;

import java.net.URL;
import java.util.ResourceBundle;

import com.calculator.igor.services.CalculatorService;
import com.calculator.igor.utils.Utils;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class MainViewController implements Initializable {
	
	private String lastResult;
	private Boolean isError = false;;
	
	//Buttons
	
	//Equals button
	@FXML
	Button btEquals;
	
	//AC / Delete Buttons
	@FXML
	Button btAc;
	
	@FXML
	Button btDelete;
	
	//TextField
	
	//ResultField 
	
	@FXML
	TextField tfResult;
	
	
	//Action events 
	
	@FXML
	public void actionEventSetDigit(ActionEvent e) {

		verifyIsError();

		
		char digit = Utils.findValueOfActionSource(e.getSource().toString());


		if(Utils.isNumeric(digit)) {
			this.setNumber(Character.toString(digit));
		}else {
			this.setOperator(Character.toString(digit));
		}
		
		
	}

	@FXML
	public void actionEventAc(ActionEvent e){
		verifyIsError();
		String text = tfResult.getText();
		char lastDigit = text.charAt(text.length() - 1);

		if(Utils.isNumeric(lastDigit)){
			if(text.length() == 1){
				tfResult.setText("0");
			}else{
				tfResult.setText(text.substring(0, text.length() -1));
			}
		}else{
			tfResult.setText(text.substring(0, text.length() - 3));
		}
	}
	
	@FXML
	public void actionEventDelete(ActionEvent e) {
		verifyIsError();
		this.tfResult.setText("0");
	}
	
	@FXML
	public void actionEventExecEquals(ActionEvent e) {
		verifyIsError();
		try {
			this.tfResult.setText(CalculatorService.execEquals(tfResult.getText()).replace('.', ','));
		} catch (Exception e1) {
			this.isError = true;
			this.tfResult.setText(e1.getMessage());
		}
	}
	
	
	//Other methods
	private void setNumber(String strDigit) {
		
		boolean isZero = Utils.textFieldIsZero(this.tfResult.getText());
		
		if(isZero) {
			this.tfResult.setText(strDigit);
		}else {
			this.tfResult.setText(tfResult.getText() + strDigit);
		}
		
		
	}
	
	private void setOperator(String strDigit) {
		if(Utils.isNumeric(this.tfResult.getText().charAt(this.tfResult.getText().length() - 1))) {
			this.tfResult.setText(this.tfResult.getText() + " " + strDigit + " ");
		}else {
			tfResult.setText(Utils.returnFormatedString(strDigit, tfResult.getText()));
		}
		
	}

	private void verifyIsError(){

		if(this.isError){
			tfResult.setText("0");
			this.isError = false;
		}
		
	}
	
	
	
	//Getters and Setters 

	public String getLastResult() {
		return lastResult;
	}

	public Button getBtDelete() {
		return btDelete;
	}

	public void setBtDelete(Button btDelete) {
		this.btDelete = btDelete;
	}

	public void setLastResult(String lastResult) {
		this.lastResult = lastResult;
	}

	public Button getBtEquals() {
		return btEquals;
	}

	public void setBtEquals(Button btEquals) {
		this.btEquals = btEquals;
	}


	public Button getBtAc() {
		return btAc;
	}

	public void setBtAc(Button btAc) {
		this.btAc = btAc;
	}

	public TextField getTfResult() {
		return tfResult;
	}

	public void setTfResult(TextField tfResult) {
		this.tfResult = tfResult;
	}
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		tfResult.setText("0");
		
	}
	

}
