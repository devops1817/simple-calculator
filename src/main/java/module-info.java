module com.calculator.igor.calculadora_simples {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.calculator.igor.calculadora_simples to javafx.fxml;
    exports com.calculator.igor.calculadora_simples;
}
