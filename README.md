<h1 align="center">
    Simple Calculator
</h1>

![Alt text](/imgs/calculatorimg.png "Optional title")

## Sobre o Projeto
Este projeto tem como objetivo, ser uma calculadora simples, porém prática. Usei o mesmo para práticar também alguns conceitos de CI / CD.

---

## Ferramentas utilizadas
- openjdk versão 17.0.3
- maven versão 3.6.3
- javafx versão 13
- junit versao 5.4.0

No arquivo <b> pom.xml </b> você poderá ver mais sobre as dependências do projeto.

---

## Como baixar e executar este projeto

```bash
#Exemplo utilizando o so linux, distribuição Ubuntu versão 20.04.4.

#Download do openjdk.
#Use as teclas simultâniamente para abrir o terminal CTRL + ALT + T.

#Com o terminal aberto, utilize o comando:
$sudo add-apt-repository ppa:openjdk-r/ppa
#Este comando adiciona o repositiorio do programa ao seu.

#Depois disso, deve usar este, para atualizar o gerenciador de pacotes.
$sudo apt-get update

#Agora é so instalar usando:
$sudo apt-get install openjdk-17-jdk

#Com o jdk instalado, agora iremos instalar o maven usando:
$sudo apt-get install -y maven 

#Depois disso verifique se o jdk e o maven foram instalados corretamente:

$ java -version
```

![Alt text](/imgs/java-installed.png "Optional title")

```bash
$ mvn -version
```

![Alt text](/imgs/maven-installed.png "Optional title")

```bash
# Com o java e o maven instalados corretamente escolha uma IDE de sua preferẽncia.

# Vou usar o eclise  neste exemplo.

# instalando o eclipse. 
$sudo snap install --classic eclipse

# Com o eclise instalado vamos clonar este projeto. 

# Clona o repositorio do gitlab para o diretorio atual
$ git clone https://gitlab.com/devops1817/simple-calculator.git


# Agora vamos para a IDE e importar o projeto maven

#File -> import
```
![Alt text](/imgs/File-Import.jpeg  "Optional title")

```bash
# Maven -> Existing Maven Projects -> Next
```
![Alt text](/imgs/Maven-Exes.png "Optional title")

```bash
# Browse
```
![Alt text](/imgs/Browse.png   "Optional title")

```bash
# Ache o diretório do projeto -> Open
```
![Alt text](/imgs/Ache-O-Projeto.png  "Optional title")



---

## Rodando o projeto

O vídeo a seguir mostra a execução do projeto: <a href = https://youtu.be/Pm8x3tcAREA> https://youtu.be/Pm8x3tcAREA </a>


