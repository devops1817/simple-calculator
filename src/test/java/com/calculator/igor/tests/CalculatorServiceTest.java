package com.calculator.igor.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.calculator.igor.services.CalculatorService;

class CalculatorServiceTest {

	@Test
	public void testSumShouldBeReturn30() {
		Assertions.assertEquals("30.0", CalculatorService.sum(25, 5));
	}

	@Test
	public void testSubtractionShouldBeReturn100() {
		Assertions.assertEquals("100.0", CalculatorService.subtraction(300, 200));
	}

	@Test
	public void testDivisionOfIntegerPositiveNumbersShouldBeReturn25() throws Exception {
		Assertions.assertEquals("25.0", CalculatorService.division(100, 4));
	}

	@Test
	public void testDivisionOfIntegerNegativeNumbersShouldBeReturn3() throws Exception {
		Assertions.assertEquals("3.0", CalculatorService.division(-6, -2));
	}

	@Test
	public void testDivisionOfIntegerPositiveAndNegativeNumbersShouldBeReturn3() throws Exception {
		Assertions.assertEquals("-3.0", CalculatorService.division(6, -2));
	}

	@Test
	public void testDivisionOfIntegerNegativeAndPositiveNumbersShouldBeReturn3() throws Exception {
		Assertions.assertEquals("-4.0", CalculatorService.division(-8, 2));
	}

	@Test
	public void testDivisionOfFloatNumbersShouldBeReturn4() throws Exception {
		Assertions.assertEquals("4.0", CalculatorService.division(1, 0.25));
	}

	@Test
	public void testMutiplicationOfIntegerPositiveNumbersShouldBeReturn25() throws Exception {
		Assertions.assertEquals("25.0", CalculatorService.division(100, 4));
	}

	@Test
	public void testMultiplicationOfIntegerNegativeNumbersShouldBeReturn3() throws Exception {
		Assertions.assertEquals("3.0", CalculatorService.division(-6, -2));
	}

	@Test
	public void testDMultiplicationOfIntegerPositiveAndNegativeNumbersShouldBeReturn3() throws Exception {
		Assertions.assertEquals("-3.0", CalculatorService.division(6, -2));
	}

	@Test
	public void testMultiplicartionOfIntegerNegativeAndPositiveNumbersShouldBeReturn3() throws Exception {
		Assertions.assertEquals("-4.0", CalculatorService.division(-8, 2));
	}

	@Test
	public void testMultiplicationOfFloatNumbersShouldBeReturn4() throws Exception {
		Assertions.assertEquals("4.0", CalculatorService.division(1, 0.25));
	}

}
